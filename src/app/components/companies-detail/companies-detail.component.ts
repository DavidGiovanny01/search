import { Component, OnInit } from '@angular/core';

import { CompanyService } from 'src/app/services/company.service';
import { Company } from 'src/app/utils/interfaces';

@Component({
  selector: 'app-companies-detail',
  templateUrl: './companies-detail.component.html',
  styleUrls: ['./companies-detail.component.scss']
})
export class CompaniesDetailComponent implements OnInit {
  selectedCompanies: Company[];

  constructor(private cs: CompanyService) { }

  ngOnInit() {
    this.selectedCompanies = this.cs.getCompanies();
    console.log('selected-companies', this.selectedCompanies);
  }

}
