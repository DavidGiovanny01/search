import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from 'src/app/services/api.service';
import { CompanyService } from 'src/app/services/company.service';

import { Country, Company, SocialNetwrok } from 'src/app/utils/interfaces'
import { companies, socialNetworks } from 'src/app/utils/fake-data'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  selectedCountry: Country;
  countries: Country[];
  socialNetworks: SocialNetwrok[];
  companies: Company[];
  search: string;
  count: number;

  constructor(
    private api: ApiService,
    private cs: CompanyService,
    private router: Router
  ) { }

  ngOnInit() {
    this.api.getCountries().subscribe((countries: Country[]) => {
      console.log('countries', countries);
      this.countries = countries;
      this.filterCompaniesByCountry();
    });

    this.selectedCountry = { name: 'México', alpha2Code: 'MX' };
    this.socialNetworks = socialNetworks;
    this.companies = companies;
    console.log('socialNetworks', this.socialNetworks);
    console.log('companies', this.companies);
    this.getCount();
  }

  onSelectCountry(country) {
    this.selectedCountry = country;
    console.log('selectedCountry', this.selectedCountry);
    this.filterCompaniesByCountry();
  }

  filterCompaniesByCountry() {
    this.socialNetworks.forEach((sn: SocialNetwrok) => {
      sn.companies = this.companies.filter((company: Company) => {
        return company.country == this.selectedCountry.alpha2Code && company.sn == sn.code;
      });
    });
  }

  onSearchChange(search: string) {
    this.search = search.toLowerCase();
    this.socialNetworks.forEach((sn: SocialNetwrok) => {
      sn.total = sn.companies.filter((company: Company) => {
        return company.name.toLowerCase().includes(this.search) ||
          company.user.toLowerCase().includes(this.search) ||
          company.extract.toLowerCase().includes(this.search)
      }).length;
      sn.total = Math.ceil(sn.total / 5);
    });
  }

  onShowCompanies() {
    let allCompanies: Company[] = [];
    allCompanies = this.companies.filter((company: Company) => {
      return company.selected;
    });
    this.cs.setCompanies(allCompanies);
    this.router.navigate(['companies']);
  }

  getCount() {
    this.count = 0;
    this.companies.forEach((company: Company) => {
      if(company.selected) this.count++;
    });
  }
}
