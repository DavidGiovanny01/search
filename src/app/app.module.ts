import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchComponent } from './components/search/search.component';
import { CompaniesDetailComponent } from './components/companies-detail/companies-detail.component';

import { ClickOutsideDirective } from './utils/click-outside.directive';

import { FilterPipe } from './utils/filter.pipe';

import { ApiService } from './services/api.service';
import { CompanyService } from './services/company.service';


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    ClickOutsideDirective,
    FilterPipe,
    CompaniesDetailComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    ApiService, 
    CompanyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
