import { Injectable, Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Company } from '../utils/interfaces';

const STORAGE_KEY = 'companies';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(@Inject(SESSION_STORAGE) private storage: StorageService) { }

  setCompanies(companies: Company[]) {
    this.storage.set(STORAGE_KEY, companies);
  }

  getCompanies(): Company[] {
    return this.storage.get(STORAGE_KEY);
  }
}
