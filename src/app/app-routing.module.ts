import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchComponent } from './components/search/search.component';
import { CompaniesDetailComponent } from './components/companies-detail/companies-detail.component';

const routes: Routes = [
  { path: '', component: SearchComponent },
  { path: 'companies', component: CompaniesDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
