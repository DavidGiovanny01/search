export interface Country {
    name: string,
    alpha2Code: string
};

export interface SocialNetwrok {
    name: string,
    code: string,
    logo: string,
    companies?: Company[],
    page?: number,
    total?: number
};

export interface Company {
    name: string,
    user: string,
    extract: string,
    logo: string,
    verified: boolean,
    country: string,
    sn: string,
    selected?: boolean
};