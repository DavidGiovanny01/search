import { Pipe, PipeTransform } from '@angular/core';

import { Company } from './interfaces';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(companies: Company[], search: string): any {
    if (!companies) return [];
    if (!search) return companies;
    search = search.toLowerCase();
    return companies.filter((company: Company) => {
      return company.name.toLowerCase().includes(search) ||
        company.user.toLowerCase().includes(search) ||
        company.extract.toLowerCase().includes(search)
    });

  }

}
